#!/usr/bin/env python

import sys
import io
from struct import *

#funkcija za provjeru da li je paket ima dobar zapis (True => ok)
def validPacket(inp):
  ok = True

  if(len(inp) != 64):
    ok = False

  for x in range(0, 8):
    if(x == 0):
      if(inp[8*x] != '\xa6' or inp[8*x+1] != '\x00' or inp[8*x+2] != '\xa0' or inp[8*x+3] != '\xff'):
        ok = False
    else:
      if(inp[8*x] != '\xa7' or inp[8*x+1] != '\x00' or inp[8*x+2] != '\xa0' or inp[8*x+3] != '\xff'):
        ok = False

  return ok

#provjera da li je NOP paket
def isNop(inp):
  nop = True
  for x in range(0, 8):
    if(inp[8*x+4] != '\xff'  or inp[8*x+5] != '\xff' or inp[8*x+6] != '\xff' or inp[8*x+7] != '\xff'):
      nop = False
  return nop

def printOut(inp):
  for x in range(0, 8):
    print hex(ord(inp[8*x])), hex(ord(inp[8*x+1])), hex(ord(inp[8*x+2])), hex(ord(inp[8*x+3])), hex(ord(inp[8*x+4])), hex(ord(inp[8*x+5])), hex(ord(inp[8*x+6])), hex(ord(inp[8*x+7]))
  print ''

def printOutCsv(inp):
  string = ""
  for x in range(0, 8):  
    string = string + str(ord(inp[8*x+4])) + ";"
    if(inp[8*x+5] == '\x00'):
      #ako je 000000
      string = string + str(0) + ";"
    else:
      #ako je FFFFFF
      string = string + str(1) + ";" 
  print string[0:-1]
  
stdin = io.open(sys.stdin.fileno(), mode='rb', closefd=False)
buf = []
try:
  byte = stdin.read(1)
  while byte != "":
    if len(buf) == 0 and byte == '\xa6':
      buf.append(byte)
    elif len(buf) > 0 and len(buf) < 64:
      buf.append(byte)
      
    byte = stdin.read(1)
    
    if len(buf) == 64:
      if(not validPacket(buf)):
        print 'NOT VALID PACKET'
    
      if(not isNop(buf)):
        printOutCsv(buf)

      del buf[:] 


except KeyboardInterrupt:
  print buf
