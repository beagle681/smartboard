#!/usr/bin/env python

import os
import platform
import glob
from subprocess import call

if platform.system() == 'Windows':
	cat = 'type '
	a = ''
else:
	cat = 'cat '
	a = './'

for f in glob.glob(os.path.join('', '*.binary')):
	cstr = cat + str(f) + ' | ' + a + 'sb.py > ' + str(f).replace('binary', 'csv')
	print cstr
	os.system(cstr)
